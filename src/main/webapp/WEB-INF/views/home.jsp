<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome!</title>
</head>
<body>

	<c:url value="/adduser" var="addUser" />
	<c:url value="/travels" var="travels"/>
	<c:url value="/logout" var="logoutUrl" />

	<h2>Hello: ${username}</h2>
	<h3>What do you want to do?</h3>
	
	<ul>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<li><a href="${addUser}">Add new user</a></li>
		</sec:authorize>
		
		<sec:authorize access="hasAnyRole('ROLE_TRAVELLER', 'ROLE_BOSS')">
			<li><a href="${travels}">Manage your travels</a></li>
		</sec:authorize>	
		
		<sec:authorize access="hasRole('ROLE_BOSS')">
			<li><a href="${travelCosts}">Check travel costs</a></li>
		</sec:authorize>
		
		<sec:authorize access="hasRole('ROLE_FILLER')">
			<li><a href="${travelRequest}">Fill in the travel requests</a></li>
		</sec:authorize>
	</ul>
	
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="submit" value="Logout"></input>	
	</form>
	
</body>
</html>