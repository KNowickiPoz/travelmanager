<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage your travels</title>
</head>
<body>
	<h2>Manage your travels</h2>
	<div>
		<table border=1 width="100%">
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Destination City</th>
					<th>Destination Address</th>
					<th>Purpose</th>
					<th>Travel time</th>
					<sec:authorize access="hasRole('ROLE_FILLER')">
					<th>Transportation type</th>
					<th>Departure time</th>
					<th>Arrival time</th>
					<th>Hotel address</th>
					<th>Return departure time</th>
					<th>Return arrival time</th>
					<th>Cost</th>
					</sec:authorize>
				</tr>
			<c:forEach var="travel" items="${travels}">
				<tr>
					<td>${travel.id}</td>
					<td>${travel.name}</td>
					<td>${travel.destinationCity}</td>
					<td>${travel.destinationAddress}</td>
					<td>${travel.purpose}</td>
					<td>${travel.travelTime}</td>
					<sec:authorize access="hasRole('ROLE_FILLER')">
					<td>blank</td>
					<td>blank</td>
					<td>blank</td>
					<td>blank</td>
					</sec:authorize>
				</tr>
			</c:forEach>
		</table>
	</div>
	<div>
		Create travel
		<table>
			<!--  
			Korzystamy tutaj z tagu dostarczonego przez deklaracje wyzej o nazwie 'form',
			pozwala on na zbudowanie formularza wedlug modelu (encji) odpowiadajacej tej, w bazie danych.
			Waznym elementem jest tutaj modelAttribute="travel", gdyz informuje on springa o tym,
			ze formularz odpowiada modelowi Travel.java. Warto zwrocic uwage, ze zadeklarowane sa
			wszystkie pola z encji, oprocz ID - tego pola nie musimy przesylac / wypelniac gdyz
			zostanie uzupelnione automatycznie - mamy zadeklarowany w encji generator oraz adnotacje @Id.
			Jesli bysmy sie chcieli przesylac jakiegos pola, np. 'name', a encja wymagalaby uzupelnienia
			takiego pola przy dodawaniu do bazy danych, musielibysmy je uzupelnic w kontrolerze, przed zapisem
			danych do bazy np. travel.setName("Jakas podroz"); w TravelController.java przy metodzie obslugujacej
			zapytanie pod tym adresem, dla danej metody przesylania (GET / POST).
			 -->
			<form:form modelAttribute="travel" action="/travelmanager/travels/addTravel" method='POST'>
				<tr>
					<td>Name:
					<!--  
					Tag springowy 'spring:bind' sluzy do mapowania atrybutow danej encji. Tak jak path="name",
					odpowiada polu 'name' w encji. Atrybut path jest wykorzystywany do mapowania nazw pol encji
					na pola w formularzu. 'form:input' zostanie automatycznie zamieniony na input, z odpowiednimi 
					atrybutami gotowymi do przeslania formularza, ktory bedzie odpowiadal encji. 
					 -->
					<spring:bind path="name">
						<form:input path="name" type="text" />
						<!-- 
						Tag 'form:errors' sluzy do wyswietlania bledow, ktore pochodza od walidatora.
						Jesli po przeslaniu formularza, zapytanie do bazy nie wykona sie ze wzgledu np.
						puste pola (a deklaracja w encji wymaga, aby takie nie byly), to w tym miejscu 
						zostana wyswietlone bledy o zadanej przez nas tresci.
						 -->
						<form:errors path="name" />
					</spring:bind>
					</td>
				</tr>
				<tr>
					<td>Destination city:
					<spring:bind path="destinationCity">
						<form:input path="destinationCity" type="text" />
						<form:errors path="destinationCity" />
					</spring:bind>
					</td>
				</tr>
				<tr>
					<td>Destination address:
					<spring:bind path="destinationAddress">
						<form:input path="destinationAddress" type="text" />
						<form:errors path="destinationAddress" />
					</spring:bind>
					</td>
				</tr>
				<tr>
					<td>Purpose:
					<spring:bind path="purpose">
						<form:input path="purpose" type="text" />
						<form:errors path="purpose" />
					</spring:bind>
					</td>
				</tr>
				<tr>
					<td>Travel time:
					<spring:bind path="travelTime">
						<form:input path="travelTime" type="datetime" />
						<form:errors path="travelTime" />
					</spring:bind>
					</td>
				</tr>
				<td><input type="submit" value="Add Travel" /></td>
			</form:form>
		</table>
	</div>
	
	<a href="${home}">Go back</a>
	
</body>
</html>