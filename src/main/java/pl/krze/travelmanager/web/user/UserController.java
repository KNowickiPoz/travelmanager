package pl.krze.travelmanager.web.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import pl.krze.travelmanager.core.user.User;
import pl.krze.travelmanager.core.user.UserDao;
import pl.krze.travelmanager.core.user.UserManager;

@Controller
public class UserController {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserManager userManager;
	
	@RequestMapping(value = "/adduser", method = RequestMethod.POST)
	public RedirectView addUser() {
		userManager.addUser(new User());
		return new RedirectView("login");
	}
	
}
