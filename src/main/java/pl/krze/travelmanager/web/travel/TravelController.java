package pl.krze.travelmanager.web.travel;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import pl.krze.travelmanager.core.travel.Travel;
import pl.krze.travelmanager.core.travel.TravelDao;
import pl.krze.travelmanager.core.travel.TravelManager;

@Controller
public class TravelController {
	
	
	@Autowired
	private TravelDao travelDao;
	
	@Autowired
	private TravelManager travelManager;
	
	@RequestMapping(value = "/travels", method = RequestMethod.GET)
	public ModelAndView travelPage() {
		ModelAndView travel = new ModelAndView("travels");
		travel.addObject("travels", travelDao.getAll());
		travel.addObject("travel", new Travel());
		return travel;
	}
	
	
	/* W tym miejscu przy uzyciu adnotacji @RequestMapping, ktora przymuje 2 argumenty
	 * definiujemy sciezke dostepu do controllera. Przykladowo, jezeli wejdziemy pod adres
	 * w przegladarce /travels/addTravel, metoda POST zostanie wywolana metoda travelAdd() 
	 * z tego konktrolera. Kontrolery rejestrowane sa przy inicjalizacji aplikacji, takze
	 * po wejsciu pod adres, aplikacja automatycznie wie, ktora metode wywolac.
	 */
	@RequestMapping(value = "/travels/addTravel", method = RequestMethod.POST)
	public String travelAdd(@ModelAttribute Travel travel) {
		/* Metoda travelAdd(), ma przekazany jeden parametr (nie wszystkie osobno)
		 * dlatego nie uzywamy tutaj adnotacji @RequestParam, ktora pozwala na pobranie
		 * jednego parametru przesylanego metoda POST, a @ModelAtrribute, ktory reprezentuje
		 * caly model atrybutow. Warto zwrocic uwage na to, ze nasz model w pliku travels.jsp
		 * dokladnie odpowiada modeoli w encji - nie przesylamy tylko pola Id, ktore
		 * zostanie uzupelnione automatycznie.
		 */
		
		/* Naszym nastepnym krokiem jest zapisanie do bazy danych przeslanego modelu 
		 * czyli calej encji. W tym celu odwolamy sie do DAO obslugujacego encje Travel
		 * i wykorzystamy do tego metode saveOrUpdate(), ktora zajmie sie zapisem nowej encji 
		 * do bazy danych
		 */
		travelDao.saveOrUpdate(travel);
		
		/* Przy zwracaniu wartosci bedziemy zwracali sam string "redirect:/travels", ktory
		 * spowoduje przeniesienie uzytkownika na strone z listowaniem podrozy oraz 
		 * formularzem dodawania nowej.
		 */
		return "redirect:/travels";
	}
	
	
}
