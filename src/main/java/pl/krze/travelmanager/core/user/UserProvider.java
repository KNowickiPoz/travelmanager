package pl.krze.travelmanager.core.user;

public interface UserProvider {

	User getLoggedUser();

	void saveLoggedUser(String username);

}