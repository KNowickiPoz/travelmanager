package pl.krze.travelmanager.core.user;

import pl.krze.travelmanager.core.BaseDao;

public interface UserDao extends BaseDao<User, Long> {
	
		User getByLogin(String login);
}
