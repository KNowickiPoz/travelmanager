package pl.krze.travelmanager.core.user;

public enum UserRole {
	
	ROLE_ADMIN,
	ROLE_BOSS,
	ROLE_TRAVELLER,
	ROLE_FILLER;
	

}
