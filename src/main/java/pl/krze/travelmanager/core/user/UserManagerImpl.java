package pl.krze.travelmanager.core.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.krze.travelmanager.core.AbstractBaseDao;

@Repository
@Transactional
public class UserManagerImpl extends AbstractBaseDao<User, Long> implements UserManager {

	@Autowired
	private UserDao userDao;
	
	@Override
	@Transactional
	public void addUser(User user) {
		userDao.saveOrUpdate(user);

	}

	@Override
	protected Class<User> supports() {
		
		return User.class;
	}

}
