package pl.krze.travelmanager.core.travel;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.krze.travelmanager.core.AbstractBaseDao;

@Repository
@Transactional
public class TravelManagerImpl extends AbstractBaseDao<Travel, Long> implements TravelManager{
	
	@Autowired
	private TravelDao travelDao;

	@Override
	@Transactional
	public void addTravel(Travel travel) {
		travelDao.saveOrUpdate(travel);
		
	}

	@Override
	public void deleteById(Long id) {
		currentSession().createQuery("delete from Travel where id=:travelId").setParameter("travelId", id).executeUpdate();
		
	}

	@Override
	protected Class<Travel> supports() {
		return Travel.class;
	}
	
	

}
