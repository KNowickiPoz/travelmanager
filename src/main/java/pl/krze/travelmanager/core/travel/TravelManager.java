package pl.krze.travelmanager.core.travel;

public interface TravelManager {
	public void addTravel(Travel travel);
	void deleteById(Long id);
}
