package pl.krze.travelmanager.core.travel;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.krze.travelmanager.core.AbstractBaseDao;

@Repository
@Transactional
public class TravelDaoImpl extends AbstractBaseDao<Travel, Long> implements TravelDao {
	
	@Override
	protected Class<Travel> supports() {
		return Travel.class;
	}
}
