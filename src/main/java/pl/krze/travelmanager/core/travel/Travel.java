package pl.krze.travelmanager.core.travel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Travel {
	
	@Id
	@GeneratedValue(generator = "travelSeq")
	@SequenceGenerator(name= "travelSeq", sequenceName = "travel_seq")
	private Long id;
	
	@JoinColumn(name = "user_id")
	@ManyToOne
	private String name;
	
	@Column
	private String destinationCity;
	
	@Column
	private String destinationAddress;
	
	@Column
	private String purpose;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column
	private Date travelTime;
	
	@Column
	private String transportationType;
	
	@Column
	private String departureTime;
	
	@Column
	private String arrivalTime;
	
	@Column
	private String returnDepTime;
	
	@Column
	private String returnArrTime;
	
	@Column
	private Double cost;

	public Travel(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public Date getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(Date travelTime) {
		this.travelTime = travelTime;
	}
	
	public String getTransportationType() {
		return transportationType;
	}

	public void setTransportationType(String transportationType) {
		this.transportationType = transportationType;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getReturnDepTime() {
		return returnDepTime;
	}

	public void setReturnDepTime(String returnDepTime) {
		this.returnDepTime = returnDepTime;
	}

	public String getReturnArrTime() {
		return returnArrTime;
	}

	public void setReturnArrTime(String returnArrTime) {
		this.returnArrTime = returnArrTime;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Travel() {

	}

}
