
create sequence user_seq;
INSERT INTO user(id, login, password, name, surname, position, department, role) values (nextval('user_seq'), 'login', 'login', 'John', 'Smith', 'Java specialist', 'Development', 'ROLE_TRAVELLER');
INSERT INTO user(id, login, password, name, surname, position, department, role) values (nextval('user_seq'), 'login1', 'login1', 'Agent', 'Smith', 'Travel specialist', 'Travel', 'ROLE_FILLER');
INSERT INTO user(id, login, password, name, surname, position, department, role) values (nextval('user_seq'), 'login2', 'login2', 'Boss', 'Smith', 'Java Boss', 'Development', 'ROLE_BOSS');
INSERT INTO user(id, login, password, name, surname, position, department, role) values (nextval('user_seq'), 'admin', 'admin', 'Admin', 'Smith', 'Technician', 'IT', 'ROLE_ADMIN');

create sequence travel_seq;
INSERT INTO travel(id, name, destinationCity, destinationAddress, purpose, travelTime) values (nextval('travel_seq'), 'first travel', 'Gdansk', 'ul. Pawlicka', 'Business', '12.07.2016');

commit;